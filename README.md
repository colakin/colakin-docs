# README #

Steps necessary to get documentation up and running.

### How do I get set up? ###

* Install python

```
$ sudo apt-get update                         
$ sudo apt-get install python3.6
```

* Install pip package manager

```
$ pip install sphinx
```

Now, edit your ``index.rst`` and add some information about your project.
Include as much detail as you like (refer to the reStructuredText if you need help). 
Build them to see how they look:

```
$ make html
```

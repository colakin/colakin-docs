AppWorkflow Configuration API
=============================


App workflow Configuration API lets you list, add, update and delete with configurations.

Using this REST API, The client(mobile application) can get the configuration on a startup and take necessary action.


.. contents:: Table of contents
   :local:
   :backlinks: none


Base URL
--------

All URLs referenced in the documentation have the following base:

.. code-block:: javascript

   http://152.67.113.43:8082/


Get an existing configuration
-----------------------------

In this guide, we'll show you how to use this API to get a configuration.

To call this REST API. we need the following parameters

.. note::
	Replace BASE_URL with the actual base URL.

:application:

	The application where the API call is originating (ex: mobile, admin-panel).

:location:

	The location from which the API call is made(ex: India, Australia).

:code:	The Configuration code which has to be retrieved.


.. http:get:: /{application}/{location}/{code}

    Retrieve a configuraion for the given application, location and code.

    **Example request**:

    .. tabs::

        .. code-tab:: bash

        	$ curl -X GET BASE_URL/Mobile/Australia/Reg100
 
 	**Example response**:

    .. sourcecode:: json

		{
		    "application": "Mobile",
		    "location": "Australia",
		    "code": "Reg100",
		    "description": "Social Login Configuration",
		    "locale": "En_UK",
		    "detail": [
		        {
		            "id": "Twitter",
		            "status": "enabled",
		            "displayName": "Twitter",
		            "i18nValue": "Twitter",
		            "authURL": "http://stet/",
		            "logo": "http:/image",
		            "position": 1
		        }
		    ]
		}



:locale: The locale of the configuration(ex: En_UK).

:description: Describes the purpose of the configuration.

:type: Describes the type of configuration(ex: Array, Object, String and etc...).

:detail: - The Actual Configuration (it can be Array, Object, String and etc...)

Add a new configuration
-----------------------

In this guide, we'll show you how to use this API to add a new Configuration.

To call this REST API. we need the following parameters

:application: The application where the configuration will be used (ex: mobile, admin-panel).

:location: The location where the configuration is applicable (ex: India, Australia).

:code: The configuration code is used to segregate different kinds of configurations.

:locale: The locale of the configuration(ex: En_UK).

:description: Describes the purpose of the configuration.

:type: Describes the type of configuration(ex: Array, Object, String and etc...).

:detail: The Actual Configuration (it can be Array, Object, String and etc...)


.. http:post:: /

    Add a configuraion for the given application, location and code.

    **Example request**:

    .. tabs::

        .. code-tab:: bash

        	$ curl -X POST BASE_URL \
  			-H 'content-type: application/json' \
  			-d @body.json

	
	The content of ``body.json`` can be like,
	
	**Array**

	.. sourcecode:: json

		{
		  "application": "Mobile",
		  "location": "Australia",
		  "code": "Reg100",
		  "locale": "En_UK",
		  "description": "Social Login Configuration",
		  "type": "Array",
		  "detail": [
		    {
		      "id": "Twitter",
		      "status": "enabled",
		      "displayName": "Twitter",
		      "i18nValue": "Twitter",
		      "authURL": "http://stet/",
		      "logo": "http:/image",
		      "position": 1
		    }
		  ]
		}

	**Object**

	.. sourcecode:: json

		{
		  "application": "Mobile",
		  "location": "Australia",
		  "code": "Reg110",
		  "locale": "En_UK",
		  "description": "Secret",
		  "type": "Object",
		  "detail": {
		      "serviceId": "95328221",
		      "secretId": "s.Pfgvsdff"
		    }
		}

	**String**

	.. sourcecode:: json

		{
		  "application": "Mobile",
		  "location": "Australia",
		  "code": "Reg101",
		  "locale": "En_UK",
		  "description": "Get OTP Button Message",
		  "type": "String",
		  "detail": "Get Opt"
		}
 
 	**Example response** based on the request:

    .. sourcecode:: json

		{
		  "application": "Mobile",
		  "location": "Australia",
		  "code": "Reg110",
		  "description": "Secret",
		  "locale": "En_UK",
		  "detail": {
		    "serviceId": "95328221",
		    "secretId": "s.Pfgvsdff"
		  }
		}


Update an existing configuration
--------------------------------

In this guide, we'll show you how to use this API to update an existing Configuration.

To call this REST API. we need the following parameters

:application: The application where the configuration will be used (ex: mobile, admin-panel).

:location: The location where the configuration is applicable (ex: India, Australia).

:code: The configuration code is used to segregate different kinds of configurations.

:locale: The locale of the configuration(ex: En_UK).

:description: Describes the purpose of the configuration.

:type: Describes the type of configuration(ex: Array, Object, String and etc...).

:detail: The Actual Configuration (it can be Array, Object, String and etc...)


.. http:put:: /

    Update a configuraion for the given application, location and code.

    **Update**

    .. sourcecode:: json

		{
		  "application": "Mobile",
		  "location": "Australia",
		  "code": "Reg101",
		  "locale": "En_UK",
		  "description": "Get OTP Button Message",
		  "type": "String",
		  "detail": "Get Opt"
		}

    **to**

    .. sourcecode:: json

		{
		  "application": "Mobile",
		  "location": "Australia",
		  "code": "Reg101",
		  "locale": "En_UK",
		  "description": "Get OTP Button Message",
		  "type": "String",
		  "detail": "Get One Time Password"
		}

    **Example request**:

    .. tabs::

        .. code-tab:: bash

        	$ curl -X PUT BASE_URL \
  			-H 'content-type: application/json' \
  			-d @body.json

	
	The content of ``body.json`` can be like,

	.. sourcecode:: json

		{
		  "application": "Mobile",
		  "location": "Australia",
		  "code": "Reg101",
		  "locale": "En_UK",
		  "description": "Get OTP Button Message",
		  "type": "String",
		  "detail": "Get One Time Password"
		}

 
 	**Example response**:

    .. sourcecode:: json

		{
		  "application": "Mobile",
		  "location": "Australia",
		  "code": "Reg101",
		  "locale": "En_UK",
		  "description": "Get OTP Button Message",
		  "type": "String",
		  "detail": "Get One Time Password"
		}


Deleting an existing configuration
----------------------------------

In this guide, we'll show you how to use this API to delete a configuration.

To call this REST API. we need the following parameters

:application: The application where the configuration is originating (ex: mobile, admin-panel).

:location: The location where the configuration is made (ex: India, Australia).

:code: The Configuration code which has to be deleted.


.. http:delete:: /{application}/{location}/{code}


    Delete a configuraion for the given application, location and code.


    **Example request**:

    .. tabs::

        .. code-tab:: bash

        	$ curl -X DELETE BASE_URL/Mobile/Australia/Reg100 \
  			-H 'content-type: application/json' \



 	**Example response**:

    .. sourcecode:: json

		{
		   "application": "Mobile",
		   "location": "Australia",
		   "code": "Reg100",
		   "description": "Social Login Configuration",
		   "locale": "En_UK",
		   "detail": [
		   {
		     "id": "Twitter",
		     "status": "enabled",
		     "displayName": "Twitter",
		     "i18nValue": "Twitter",
		     "authURL": "http://stet/",
		     "logo": "http:/image",
		     "position": 1
		   }
		  ]
		}

Authentication API's
====================


Authentication REST API's (Application Programming Interface) lets Authentication and Authorise you communicate with Colakins services.

.. note::
    The API requests and responses are formatted in JSON.


.. contents:: Table of contents
   :local:
   :backlinks: none

Prerequisites
-------------

RoleId, SecretId (which is received from a secure source (admin/ build system))


Base URL's
--------

All URLs referenced in the documentation have the following base:

.. code-block:: javascript

    APPLICATION_AUTH_SERVICE : http://152.67.101.131:9004

    CUSTOMER_AUTH_SERVICE :  http://152.67.101.131:9001

    USER_AUTH_SERVICE :  http://152.67.113.43:8081


Get an application token
-----------------------------

Now that we have RoleId and SecretId, we can go ahead and create an application token. to do that we run this API call. Remember to replace YOUR_ROLE_ID and YOUR_SECRET_ID with your RoleId and SecretID.

To call this REST API. we need the following parameters

.. note::
    Replace APPLICATION_AUTH_SERVICE with the actual authentication service url.

:YOUR_ROLE_ID:

    Generated role for accessing application authentication services.

:YOUR_SECRET_ID:

    Generated secret for authenticating againts application autentication services.


.. http:post:: APPLICATION_AUTH_SERVICE/v1/auth/approle/login

    Autenticate and Retrieve an access token from application authentication service.

    **Example request**:

    .. tabs::

        .. code-tab:: bash

            $ curl --request POST \
            APPLICATION_AUTH_SERVICE/v1/auth/approle/login
            -d @body.json


    The content of ``body.json`` can be like,

    .. sourcecode:: json

        {
          "role_id": "YOUR_ROLE_ID",
          "secret_id": "YOUR_SECRET_ID"
        }
 
    **Example response**:

    .. sourcecode:: json

        {
          "request_id": "3f6f88d2-a70e-167e-3b41-cb4dffaf6dff",
          "lease_id": "",
          "renewable": false,
          "lease_duration": 0,
          "auth": {
            "client_token": "s.QiC3v6gqhWDQC3vHM1HHFRpY",
            "accessor": "PKoN1meaktfyYpCc1eDQeDcC",
            "policies": ["default", "rest"],
            "token_policies": ["default", "rest"],
            "metadata": {
               "role_name": "rest",
               "tag1": "development"
            },
          "lease_duration": 3600,
          "renewable": true,
          "entity_id": "1b3ce76e-d3dd-bff4-09e4-399759a140de",
          "token_type": "service",
          "orphan": true
         }
        }

Now remember the application authentication token (in this case: "s.QiC3v6gqhWDQC3vHM1HHFRpY") and proceed to next step.

Get customer credentials
------------------------

Now that we have an application authentication token, we can go ahead and retrieve customer credentials. to do that we run this API call. Remember to replace YOUR_APPLICATION_TOKEN with your Application Token.

To call this REST API. we need the following parameters

:YOUR_APPLICATION_TOKEN: The application authentication, `See Get an application token <#get-an-application-token>`_ for more information.
:application: is the application in need of customer credentials. here we are getting credentials for the mobile application.
:profile: is the configuration profile we want to load.

.. http:get:: APPLICATION_AUTH_SERVICE/v1/{application}/data/{profile}

    Get customer credentials for the given application

    **Example request**:

    .. tabs::

        .. code-tab:: bash

            $ curl --header "X-Vault-Token: s.QiC3v6gqhWDQC3vHM1HHFRpY" \
            --request GET APPLICATION_AUTH_SERVICE/v1/mobile/data/development
 
    **Example response**:

    .. sourcecode:: json

        {
          "request_id": "bad1cf8c-388c-88ed-b631-fca3dc7e0926",
          "lease_id": "",
          "renewable": false,
          "lease_duration": 0,
          "data": {
            "data": {
               "clientId": "mobile",
               "clientSecret": "8371688e-b33d-46db-b81a-0a527eebd688"
            },
            "metadata": {
              "created_time": "2020-02-14T06:18:45.7925395Z",
              "deletion_time": "",
              "destroyed": false,
              "version": 1
            }
        },
          "wrap_info": null,
          "warnings": null,
          "auth": null
        }

Now remember the customer credentials (in this case: "clientId": "mobile" and "clientSecret": "8371688e-b33d-46db-b81a-0a527eebd688") and proceed to next step.

Get a customer token
--------------------

Now that we have customer credentials, we can go ahead and retrieve customer token. to do that we run this API call. Remember to replace YOUR_CLIENT_ID with your ClientId and YOUR_CLIENT_SECRET with your ClientSecret.

To call this REST API. we need the following parameters

:customer: Unique customer in colakin platform.
:YOUR_CLIENT_ID:  Customer id for retrieving client token.
:YOUR_CLIENT_SECRET: Customer secret for retrieving client token.


.. http:get:: CUSTOMER_AUTH_SERVICE/auth/realms/{customer}/protocol/openid-connect/token

    Getting customer token.

    **Example request**:

    .. tabs::

        .. code-tab:: bash

            $ curl --data "grant_type=client_credentials&client_id=YOUR_CLIENT_ID&client_secret=YOUR_CLIENT_SECRET" \
            CUSTOMER_AUTH_SERVICE/auth/realms/colakin/protocol/openid-connect/token

 
    **Example response**:

    .. sourcecode:: json

        {
          "access_token": "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJMQ1FyLXBsVk05NFN0bDg1WnJiRUNHaGxhZVpXNVpLaVlVZWlkZkROSTVFIn0.eyJqdGkiOiI1ODJmZGM5OC1kYmIwLTQwMDQtYmE4ZC0yMzZhYzExNjdjZGUiLCJleHAiOjE2MTI5NjA5MDYsIm5iZiI6MCwiaWF0IjoxNTgxNDI0OTA2LCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvYXV0aC9yZWFsbXMvY29sYWtpbi5jb20iLCJhdWQiOiJhY2NvdW50Iiwic3ViIjoiZDU2NmVkOGQtNTU4OC00NGNkLThhOTktZDBlZjJkNTM3ZDc0IiwidHlwIjoiQmVhcmVyIiwiYXpwIjoiYXV0aC1zZXJ2ZXIiLCJhdXRoX3RpbWUiOjAsInNlc3Npb25fc3RhdGUiOiJiMzkzMzM1OC04NzVkLTQ4ZTUtODljMC0zMjI4NWE0MzkwODAiLCJhY3IiOiIxIiwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwidW1hX2F1dGhvcml6YXRpb24iXX0sInJlc291cmNlX2FjY2VzcyI6eyJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJzY29wZSI6ImVtYWlsIHByb2ZpbGUiLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsInByZWZlcnJlZF91c2VybmFtZSI6Iis5MTg4NzA2MjM0NTk6bW9iaWxlIn0.hPWTFzPJdCbT25rbkxmotjQpUcdvdCxkwldu459AkXOlA_lbuN7cNXD674ej8tlWG18SkP2PMeE-QIXYfgst5CwYUCbKaOhPQC36WHFW-oUGud2wLilj60CrOXEsPtty06vheCe7Dd7il0QQCOc7mZkcuUyE3QXnuSrGashyGTp-8x7BlGteCGg29mS2hBHuBouy8NC-uK8EhEakoWKByt0plWrcCkw7FzrD7e95r69qLVyFPE_fiw_KZbaw9klbGESKmi8B_HRYVm0KDen89N3cMBUKX3j792ami40gM3enV9QV_XyjjzzQUITw7E97FkKxBrZbKcDiAWio2n8VZg",
          "expires_in": 300,
          "refresh_expires_in": 1800,
          "refresh_token": "eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJlNWNmYWEwZS0zN2I3LTQ2YTItOTQ5MS1hM2JiNDIwMDY4NWYifQ.eyJqdGkiOiJlZmNlNmNiYi1lNTI5LTQ2NzEtYWQyOC1mYjM1NGU0ZWUyYjUiLCJleHAiOjE2MTI5NjA5MDYsIm5iZiI6MCwiaWF0IjoxNTgxNDI0OTA2LCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvYXV0aC9yZWFsbXMvY29sYWtpbi5jb20iLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvYXV0aC9yZWFsbXMvY29sYWtpbi5jb20iLCJzdWIiOiJkNTY2ZWQ4ZC01NTg4LTQ0Y2QtOGE5OS1kMGVmMmQ1MzdkNzQiLCJ0eXAiOiJSZWZyZXNoIiwiYXpwIjoiYXV0aC1zZXJ2ZXIiLCJhdXRoX3RpbWUiOjAsInNlc3Npb25fc3RhdGUiOiJiMzkzMzM1OC04NzVkLTQ4ZTUtODljMC0zMjI4NWE0MzkwODAiLCJyZWFsbV9hY2Nlc3MiOnsicm9sZXMiOlsib2ZmbGluZV9hY2Nlc3MiLCJ1bWFfYXV0aG9yaXphdGlvbiJdfSwicmVzb3VyY2VfYWNjZXNzIjp7ImFjY291bnQiOnsicm9sZXMiOlsibWFuYWdlLWFjY291bnQiLCJtYW5hZ2UtYWNjb3VudC1saW5rcyIsInZpZXctcHJvZmlsZSJdfX0sInNjb3BlIjoiZW1haWwgcHJvZmlsZSJ9.-gmtJdnu3pZ5BcPN6IGJwTiHyfBLqkJkE_JA5lXo6tY",
          "token_type": "bearer",
          "not-before-policy": 0,
          "session_state": "b3933358-875d-48e5-89c0-32285a439080",
          "scope": "email profile"
        }

Now remember the customer access token (in this case: "access_token") and proceed to the next step.

Get user token
--------------

Now that we have a customer token, we can go ahead and retrieve the user token. to do that we run this API call. Remember to replace YOUR_CUSTOMER_ACCESS_TOKEN with your Customer Token.

To call this REST API. we need the following parameters

:YOUR_CUSTOMER_ACCESS_TOKEN: Customer access token, `See Get a customer token <#get-a-customer-token>`_ for more information.
:userId: a unique identifier which identifies a user
:userIdentity: The type of identity provider from which the user is onboarding


.. http:post:: USER_AUTH_SERVICE/otp/generate


    Getting user token.


    **Example request**:

    .. tabs::

        .. code-tab:: bash

            $ curl -X POST USER_AUTH_SERVICE/otp/generate \
            -H 'client-token: YOUR_CUSTOMER_ACCESS_TOKEN'
            -d @body.json


    The content of ``body.json`` can be like,

    **For mobile number +919876543210**

    .. sourcecode:: json

        {
          "userId": "+919876543210",
          "userIdentity": "Mobile"
        }

    **For Whatsapp number +919876543210**

    .. sourcecode:: json

        {
          "userId": "+919876543210",
          "userIdentity": "WhatsApp"
        }


    **Example response**:

    .. sourcecode:: json

        {
          "access_token": "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJFcHVrUGV5ejY4bmk1QzRQdFp3VkUtUUo0b0tNX2JCNldTcmV2X1dURi1NIn0.eyJqdGkiOiJmZDhhMTlhMi04YzQ5LTQ4OTktODEwMS00NjBmZWQ4M2I1ZmIiLCJleHAiOjE1ODIzMzk3NDAsIm5iZiI6MCwiaWF0IjoxNTgyMjUzMzQwLCJpc3MiOiJodHRwOi8vMTUyLjY3LjEwMS4xMzE6OTAwMS9hdXRoL3JlYWxtcy9jb2xha2luIiwiYXVkIjoiYWNjb3VudCIsInN1YiI6ImFjY2QxNmU5LTgyOTMtNDBkNy04NjJlLTcwMzgwZjdhYmM1MSIsInR5cCI6IkJlYXJlciIsImF6cCI6ImF1dGgtc2VydmVyIiwiYXV0aF90aW1lIjowLCJzZXNzaW9uX3N0YXRlIjoiZGUwZmRlYWEtMzlhMC00OWU4LTgxMGMtM2EwMzMzZDBjNGU2IiwiYWNyIjoiMSIsInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJwcm9maWxlIGVtYWlsIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiIrOTE4ODcwNjIzNDU4OndoYXRzYXBwIn0.FSXJfe_iCCeCRykfOUrKar0BW1xmkVu6WiKNPAmeiVbbE_WamR4NefgNE5kBH8MRUsLjPDmSufDOT0K5-rNbHt2yG8dRkzMSXIMN57OJt5EFdQvs0lTfsULZrQzCZQha6Layt8teyBcuyllZE9RiqqsGNK0_HSPxMEvYf67DMqDrf0gxWv2GV1hPg-X7qRHxrRArIiJ46YCMVBOq-O_ApFW3FagzlM3v3MWTpidEbknDdb5zU-svfW42dJI2p5_HwJjkkXx7mB_Qc-RfDEawz0wHhm0aLIkxLb6tzQLajy02svHnK3p3Nh09fFVencIjCrN4uNTbL3tHb3ug6nTWOg",
          "expires_in": 86400,
          "refresh_expires_in": 86400,
          "refresh_token": "eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICI5MGI2ODBhOC0wNzAxLTQwZWUtOGIyYy04NDc2YzNmMjhmMjYifQ.eyJqdGkiOiI3NWIxYWM1MC0zYjU0LTQ2N2UtYTg2OC1iOGMzODg1NzE5MTMiLCJleHAiOjE1ODIzMzk3NDAsIm5iZiI6MCwiaWF0IjoxNTgyMjUzMzQwLCJpc3MiOiJodHRwOi8vMTUyLjY3LjEwMS4xMzE6OTAwMS9hdXRoL3JlYWxtcy9jb2xha2luIiwiYXVkIjoiaHR0cDovLzE1Mi42Ny4xMDEuMTMxOjkwMDEvYXV0aC9yZWFsbXMvY29sYWtpbiIsInN1YiI6ImFjY2QxNmU5LTgyOTMtNDBkNy04NjJlLTcwMzgwZjdhYmM1MSIsInR5cCI6IlJlZnJlc2giLCJhenAiOiJhdXRoLXNlcnZlciIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6ImRlMGZkZWFhLTM5YTAtNDllOC04MTBjLTNhMDMzM2QwYzRlNiIsInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJwcm9maWxlIGVtYWlsIn0.Txglt1PQgAO5rKBUeuj9Jc2ch0w6kMUOqGXjTXjIzoo",
          "token_type": "bearer",
          "id_token": null,
          "not-before-policy": 0,
          "session_state": "de0fdeaa-39a0-49e8-810c-3a0333d0c4e6",
          "scope": "profile email"
        }

.. note::

    This API will send one time password to the user mobile/whatsapp based on given request.

Now that we get the User token (in this case: access_token). use this for next step

Activate user using One time password
-------------------------------------

Now that we have a customer token, and user token we can go ahead and authenticate user with one time password. to do that we run this API call. Remember to replace YOUR_CUSTOMER_ACCESS_TOKEN with your Customer Token and USER_ACCESS_TOKEN with your User Token.

To call this REST API. we need the following parameters

:YOUR_CUSTOMER_ACCESS_TOKEN: Customer access token, `See Get a customer token <#get-a-customer-token>`_ for more information.
:YOUR_USER_ACCESS_TOKEN: User access token, `See Get user token <#get-user-token>`_ for more information.
:YOUR_ONE_TIME_PASSWORD: One time password received by user from mobile/whatsapp messaging service.

.. http:post:: USER_AUTH_SERVICE/otp/verify


    Verify user with one time password.


    **Example request**:

    .. tabs::

        .. code-tab:: bash

            $ curl -X POST USER_AUTH_SERVICE/otp/generate \
            -H 'client-token: YOUR_CUSTOMER_ACCESS_TOKEN'
            -H 'Authorization: Bearer YOUR_USER_ACCESS_TOKEN'
            -d @body.json


    The content of ``body.json`` can be like,

    .. sourcecode:: json

        {
          "oneTimePassword": "YOUR_ONE_TIME_PASSWORD"


    **Example response**:

    .. sourcecode:: json

        {
          "message": "OTP verified successfully",
          "code": 200
        }


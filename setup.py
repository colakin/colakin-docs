from setuptools import setup

setup(
    name='Colakin',
    version='0.0.1',
    author='Colakin',
    install_requires=[
        "sphinx-rtd-theme",
        "sphinx-tabs",
        "sphinxcontrib-httpdomain",
        "sphinx-prompt",
        "recommonmark"
    ],
)